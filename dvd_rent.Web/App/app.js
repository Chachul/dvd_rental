﻿alert('start');


(function () {
    'use strict';

    var ExampleApp = angular.module('Example', [
        'ui.router',
        'ui.bootstrap',
        'Example.ApplicationControllers',
        'Example.ClientControllers',
        'Example.ClientServices',
        'Example.MovieControllers',
        'Example.MovieServices',
        'Example.MovieCopyControllers',
        'Example.MovieCopyServices',
        'Example.MovieCopyClientControllers',
        'Example.MovieCopyClientServices'
    ]);

    ExampleApp.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('clients',
                {
                    url: '/clients',
                    templateUrl: 'app/components/client/list.html',
                    controller: 'ClientsController',
                    resolve: {
                         clientService: 'clientService',
                    }
                });
            $stateProvider
                .state('movies',
                {
                    url: '/movies',
                    templateUrl: 'app/components/movie/list.html',
                    controller: 'MoviesController',
                    resolve: {
                        movieService: 'movieService',
                    }
                });
            $stateProvider
                .state('moviecopies',
                {
                    url: '/moviecopies',
                    templateUrl: 'app/components/moviecopy/list.html',
                    controller: 'MovieCopiesController',
                    resolve: {
                        movieCopyService: 'movieCopyService',
                    }
                });
            $stateProvider
               .state('moviecopyclients',
               {
                   url: '/moviecopyclients',
                   templateUrl: 'app/components/moviecopyclient/list.html',
                   controller: 'MovieCopyClientsController',
                   resolve: {
                       movieCopyClientService: "movieCopyClientService",
                   }
               });
            $urlRouterProvider.otherwise('clients');
        }
    ]);
})();