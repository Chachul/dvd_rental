﻿alert('jestem w movieControllers.js');


(function () {
    var exampleApp = angular.module('Example.MovieControllers', []);

    exampleApp.controller('MoviesController', function ($scope, $rootScope, $http, movieService) {
        $scope.movies = [];

        movieService.getMovies()
            .then(function(res) {
                console.log(res);
                $scope.movies = res;
            });
    });


})();