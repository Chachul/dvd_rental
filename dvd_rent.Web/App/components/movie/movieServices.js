﻿(function () {
    'use strict';
    var exampleApp = angular.module("Example.MovieServices", []);
    exampleApp.factory('movieService', function ($http, $log) {
        return {
            getMovies: function () {
                var url = 'api/movie';
                return $http.get(url)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        $log.error('Failed for getMovies.' + error.data);
                        return error.status;
                    });
            },
        }
    });
})();