﻿(function () {
    "use strict";
    var exampleApp = angular.module("Example.MovieCopyServices", []);
    exampleApp.factory("movieCopyService", function ($http, $log) {
        return {
            getMovieCopies: function () {
                var url = "api/moviecopy";
                return $http.get(url)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        $log.error("Failed for getMovieCopies." + error.data);
                        return error.status;
                    });
            },
        }
    });
})();