﻿//alert('jestem w movieCopyClientControllers.js');


//(function () {
//    var exampleApp = angular.module('Example.MovieCopyClientControllers', []);

//    exampleApp.controller('MovieCopyClientsController', function ($scope, $rootScope, $http, clientService) {
//        $scope.moviecopyclients = [];

//        clientService.getMovieCopyClients()
//            .then(function (res) {
//                console.log(res);
//                $scope.moviecopyclients = res;
//            });
//    });


//})();

(function () {
    var exampleApp = angular.module('Example.MovieCopyClientControllers', []);

    exampleApp.controller('MovieCopyClientsController', function ($scope, $rootScope, $http, movieCopyClientService) {
        $scope.moviecopyclients = [];

        movieCopyClientService.getMovieCopyClients().then(function (res) {
            console.log(res);
            $scope.moviecopyclients = res;
        });
    });
})();