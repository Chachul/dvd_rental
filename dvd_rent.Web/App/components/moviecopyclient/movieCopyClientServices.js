﻿(function () {
    "use strict";
    var exampleApp = angular.module("Example.MovieCopyClientServices", []);
    exampleApp.factory("movieCopyClientService", function ($http, $log) {
        return {
            getMovieCopyClients: function () {
                var url = "api/moviecopyclient";
                return $http.get(url)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        $log.error("Failed for getMovieCopyClients." + error.data);
                        return error.status;
                    });
            },
        }
    });
})();

