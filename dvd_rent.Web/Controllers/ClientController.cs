﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using dvd_rent.Web.Models;
using Dapper;

namespace dvd_rent.Web.Controllers
{
    public class ClientController : ApiController
    {                       
           [HttpGet]
        public IHttpActionResult GetClients()
        {
            var connectionString =
                ConfigurationManager
                .ConnectionStrings["DefaultConnection"]
                .ConnectionString;

            var clients = new List<Client>();
            using (var connection = new SqlConnection(connectionString))
            {
                clients = connection.Query<Client>("Select * From client").ToList();
            }

            return Ok(clients);

        }
    }
}