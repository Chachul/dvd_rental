﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using dvd_rent.Web.Models;
using Dapper;

namespace dvd_rent.Web.Controllers
{
    public class MovieController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetMovies()
        {
            var connectionString =
                ConfigurationManager
                .ConnectionStrings["DefaultConnection"]
                .ConnectionString;

            var movies = new List<Movie>();
            using (var connection = new SqlConnection(connectionString))
            {
                movies = connection.Query<Movie>("Select * From movie").ToList();
            }

            return Ok(movies);

        }
    }
}