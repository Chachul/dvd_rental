﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using dvd_rent.Web.Models;
using Dapper;

namespace dvd_rent.Web.Controllers
{
    public class MovieCopyClientController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetMovieCopyClients()
        {
            var connectionString =
                ConfigurationManager
                .ConnectionStrings["DefaultConnection"]
                .ConnectionString;

            var moviecopyclients = new List<MovieCopyClient>();
            //using (var connection = new SqlConnection(connectionString))
            //{
            //    moviecopyclients = connection.Query<MovieCopyClient>("Select * From moviecopyclient").ToList();
            //}

            using (var connection = new SqlConnection(connectionString))
            {
                moviecopyclients = connection.Query<MovieCopyClient>(
                    @"select mcc.Id, c.FirstName, c.LastName, m.Title, mc.SerialNumber, mcc.TakeDate, mcc.BackDate 
                        from [dbo].[MovieCopyClient] mcc
                        join [dbo].[Client] c on c.Id = mcc.ClientId
                        join [dbo].[MovieCopy] mc on mc.Id = mcc.MovieCopyId
                        join [dbo].[Movie] m on m.Id = mc.MovieId"
                ).ToList();
            }

            return Ok(moviecopyclients);

        }
    }
}