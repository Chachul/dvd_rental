﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using dvd_rent.Web.Models;
using Dapper;

namespace dvd_rent.Web.Controllers
{
    public class MovieCopyController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetMovieCopies()
        {
            var connectionString =
                ConfigurationManager
                .ConnectionStrings["DefaultConnection"]
                .ConnectionString;

            var moviecopies = new List<MovieCopy>();
            using (var connection = new SqlConnection(connectionString))
            {
                moviecopies = connection.
                    Query<MovieCopy>(@"Select * From moviecopy c join movie m on m.id = c.movieid").
                    ToList();
            }
            

                //"Select pr.MovieId, pr.SerialNumber, p.Title, pr.BuyDate, pr.TrashDate, pr.IsOnStock from moviecopy pr left join movie p on p.Id = pr.MovieId"

            return Ok(moviecopies);

        }
    }
}