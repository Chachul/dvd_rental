﻿namespace dvd_rent.Web.Models
{
    public class Client
    {
        public int  Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}