/// <autosync enabled="true" />
/// <reference path="../app/app.js" />
/// <reference path="../app/components/application/applicationcontrollers.js" />
/// <reference path="../app/components/client/clientcontrollers.js" />
/// <reference path="../app/components/client/clientservices.js" />
/// <reference path="../app/components/movie/moviecontrollers.js" />
/// <reference path="../app/components/movie/movieservices.js" />
/// <reference path="../app/components/moviecopy/moviecopycontrollers.js" />
/// <reference path="../app/components/moviecopy/moviecopyservices.js" />
/// <reference path="../app/components/moviecopyclient/moviecopyclientcontrollers.js" />
/// <reference path="../app/components/moviecopyclient/moviecopyclientservices.js" />
/// <reference path="angular.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-ui/ui-bootstrap.min.js" />
/// <reference path="angular-ui/ui-bootstrap-tpls.min.js" />
/// <reference path="angular-ui-router.min.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
